#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>

#include "tfs.h"
#include "file.h"
#include "dirent.h"

static void mark_used_blocks(struct tfs_sb_info *sbi)
{
	int i = 0;
	char *buf = malloc(sbi->s_block_size);

	memset(buf, 0, sbi->s_block_size);
	for (; i < sbi->s_data_area; i++)
		set_bit(buf, i);
	tfs_bwrite(sbi, sbi->s_block_bitmap, buf);
}

static int root_init(struct tfs_sb_info *sbi)
{
	int res = 0;
	int dirty = 0;
	struct inode *root = tfs_root_init(sbi);

	if (!root) {
		printf("ERROR: failed to get root inode!\n");
		res = -1;
		goto out_nofree;
	}

	res = tfs_add_entry(sbi, root, ".", root->i_ino, &dirty);
	if (res == -1) {
		TFS_DEBUG("trying to add '.' under %s failed!\n", "/");
		goto out;
	}

	res = tfs_add_entry(sbi, root, "..", root->i_ino, &dirty);
	if (res == -1) {
		TFS_DEBUG("trying to add .. under %s failed!\n", "/");
		goto out;
	}
	
	if (dirty)
		tfs_iwrite(sbi, root);

out:
	free_inode(root);
out_nofree:
	return res;
}

void tfs_mkfs(char *image, uint32_t offset)
{
	struct tfs_super_block sb;
	struct tfs_sb_info *sbi;
	int block_size;
	int block_bitmap_count, inode_bitmap_count, inode_table_count;
	uint32_t fs_size;

	fs_size = open_fs(image);

	sbi = malloc(sizeof(*sbi));
	if (!sbi) {
		printf("malloc for sbi failed!\n");
		return;
	}

	memset(&sb, 0, sizeof sb);
	memset(sbi, 0, sizeof(*sbi));
	sb.s_magic = TFS_MAGIC;
	sb.s_block_shift  = sbi->s_block_shift = 10;	/* 1K */
	sbi->s_block_size  = block_size = 1 << sb.s_block_shift;
	sb.s_blocks_count = sbi->s_blocks_count = fs_size / block_size;

	
	/* A half of blocks count */
	sb.s_inodes_count = sbi->s_inodes_count = sb.s_blocks_count / 2;

	sbi->s_block_bitmap_count = block_bitmap_count = roundup(sb.s_blocks_count, (block_size * 8));
	sbi->s_inode_bitmap_count = inode_bitmap_count = roundup(sb.s_inodes_count, (block_size * 8));
	sbi->s_inode_table_count  = inode_table_count  = sb.s_inodes_count * sizeof(struct tfs_inode) / block_size;
	
	sb.s_inode_bitmap = sbi->s_inode_bitmap = 0;
	sb.s_block_bitmap = sbi->s_block_bitmap = inode_bitmap_count;
	sb.s_inode_table  = sbi->s_inode_table  = sb.s_block_bitmap + block_bitmap_count;
	sb.s_data_area    = sbi->s_data_area    = sb.s_inode_table + inode_table_count;

	/* Skip the boot sector and super block sector */
	sb.s_offset       = sbi->s_offset       = offset + 2;

	sb.s_free_inodes_count = sbi->s_free_inodes_count = sb.s_inodes_count;
	sb.s_free_blocks_count = sbi->s_free_blocks_count = sb.s_blocks_count - sb.s_data_area;

	sbi->s_inodes_per_block = sbi->s_block_size / sizeof(struct tfs_inode);


	mark_used_blocks(sbi);

	cache_init(sbi);
	root_init(sbi);
	
	/* Write the suber block back to image */
	write_sector(sbi->s_offset - 1, &sb, 1);
}


int main(int argc, char *argv[])
{
	int size = sizeof(struct tfs_inode);
	int offset = 0;
	char cmd[64];
	struct tfs_sb_info *sbi;

#if 0
	printf("inode size: %d(0x%x)\n", size, size);

	size = sizeof(struct tfs_dir_entry);
	printf("dentry size: %d(0x%x)\n", size, size);

	size = sizeof(struct tfs_super_block);
	printf("super block size: %d(0x%0x)\n", size, size);
#endif
	
	if (argc < 3) {
		printf("Usage: mktfs image offset\n");
		return 1;
	}
	
	tfs_mkfs(argv[1], offset);

	return 0;
}

