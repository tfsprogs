CFLAGS = -g -Wall -Wno-unused
all: mktfs tfsh

mktfs: mktfs.c cache.c utils.c dir.c ialloc.c balloc.c inode.c file.c tfs.h dirent.h cache.h
	gcc ${CFLAGS} -o mktfs mktfs.c utils.c dir.c ialloc.c balloc.c cache.c inode.c file.c

tfsh: shell.c cache.c utils.c dir.c ialloc.c balloc.c inode.c super.c file.c tfs.h dirent.h cache.h
	gcc ${CFLAGS} -o tfsh shell.c utils.c dir.c ialloc.c balloc.c cache.c inode.c super.c file.c

new:
	qemu-img create tfs.img 1M

mkfs: new
	./mktfs tfs.img 0 design

clean:
	rm -f mktfs tfsh *.o
