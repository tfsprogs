#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>

#include "tfs.h"

static int fd;

/* 
 * Open the fs image and return the size of the fs image
 */
uint32_t open_fs(char *fs)
{
	struct stat stat;

	fd = open(fs, O_RDWR);
	if (fd < 0) {
		printf("open error\n");
		return;
	}
	fstat(fd, &stat);

	return stat.st_size;
}

/*
 * Set the bit and return it's original value
 */
int set_bit(void * addr, unsigned int nr)
{
	int		mask, retval;
	unsigned char	*ADDR = (unsigned char *) addr;

	ADDR += nr >> 3;
	mask = 1 << (nr & 0x07);
	retval = mask & *ADDR;
	*ADDR |= mask;
	return retval;
}

/* 
 * Clear the bit and return it's original value
 */
int clear_bit(void * addr, unsigned int nr)
{
	int		mask, retval;
	unsigned char	*ADDR = (unsigned char *) addr;

	ADDR += nr >> 3;
	mask = 1 << (nr & 0x07);
	retval = mask & *ADDR;
	*ADDR &= ~mask;
	return retval;
}

int test_bit(const void * addr, unsigned int nr)
{
	int			mask;
	const unsigned char	*ADDR = (const unsigned char *) addr;

	ADDR += nr >> 3;
	mask = 1 << (nr & 0x07);
	return (mask & *ADDR);
}

uint32_t find_first_zero(void *buf, void *end)
{
	uint32_t *p = (uint32_t *)buf;
	uint32_t block = 0;
	int i;

	while (*p == 0xffffffff && (void *)p < end) {
		p++;
		block += 32;
	}
	if ((void *)p >= end)
		return -1;

	for (i = 0; i < 32; i++)
		if (test_bit(p, i) == 0)
			return block + i;
}

int read_sector(uint32_t sector, void *buf, int counts)
{
	if (lseek(fd, sector << 9, SEEK_SET) < 0) {
		printf("seeking sector: %d error...\n", sector);
		return;
	}

	return read(fd, buf, 512 * counts);
}

int write_sector(uint32_t sector, void *buf, int counts)
{
	if (lseek(fd, sector << 9, SEEK_SET) < 0) {
		printf("seeking sector: %d error...\n", sector);
		return;
	}

	return write(fd, buf, 512 * counts);
}

int tfs_bread(struct tfs_sb_info *sbi, uint32_t block, void *buf)
{
	uint32_t sector = (block << (sbi->s_block_shift - 9)) + sbi->s_offset;

	return read_sector(sector, buf, 1 << (sbi->s_block_shift - 9));
}

int tfs_bwrite(struct tfs_sb_info *sbi, uint32_t block, void *buf)
{
	uint32_t sector = (block << (sbi->s_block_shift - 9)) + sbi->s_offset;

	return write_sector(sector, buf, 1 << (sbi->s_block_shift - 9));
}

