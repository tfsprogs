#include <stdio.h>
#include <string.h>
#include <malloc.h>
#include <unistd.h>
#include <fcntl.h>
#include<sys/stat.h>
#include "tfs.h"
#include "file.h"
#include "dirent.h"

static struct file * tfs_file_open(struct tfs_sb_info *sbi, char *filename, uint32_t flags)
{
	struct file *fobj;
	char *file = filename;

	fobj = tfs_open(sbi, file, flags);
	if (!fobj) {
		printf("tfs_open: open file %s error!\n", filename);
		return NULL;
	}

	return fobj;
}

static void cd(struct tfs_sb_info *sbi, char *dst_dir)
{
	DIR *old = this_dir;

	if (*dst_dir) {
		this_dir = tfs_opendir(sbi, dst_dir);	
		if (!this_dir) {
			printf("cd: %s: no such directory\n", dst_dir);
			this_dir = old;
			return;
		}

		if (this_dir->dd_dir->inode->i_mode != TFS_DIR) {
			printf("cd: %s: is not a directory\n", dst_dir);
			tfs_closedir(this_dir);
			this_dir = old;
		} else {
			tfs_closedir(old);
		}
	}
}


static void cat(struct tfs_sb_info *sbi, char *filename)
{
	struct file *file = tfs_file_open(sbi, filename, 0);
	char buf[1024];
	int bytes_read;

	if (!file)
		return;
	while ((bytes_read = tfs_read(file, buf, sizeof(buf))) > 0)
		write(1, buf, bytes_read);
}

static void touch(struct tfs_sb_info *sbi, char *filename)
{
	struct file *file = tfs_file_open(sbi, filename, LOOKUP_CREATE);
}

static void ls(struct tfs_sb_info *sbi, char *filename)
{
	DIR *dir = tfs_opendir(sbi, filename);
	struct dirent *de;
	
	if (!dir) {
		printf("open file or dir %s failed!\n", filename);
		return;
	}

	if (dir->dd_dir->inode->i_mode == TFS_FILE) {
		printf("%d\t %s\n", dir->dd_dir->inode->i_ino, filename);
		tfs_closedir(dir);
		return;
	}

	while ((de = tfs_readdir(dir))) {
		printf("%6d\t %s\n", de->d_ino, de->d_name);
		free(de);
	}

	tfs_closedir(dir);
}

static void cmd_mkdir(struct tfs_sb_info *sbi, char *filename)
{
	tfs_mkdir(sbi, filename);
}

static void cmd_rmdir(struct tfs_sb_info *sbi, char *filename)
{
	tfs_rmdir(sbi, filename);
}

void cp(struct tfs_sb_info *sbi, char *from, char *to)
{
	char buf[1024];
	int count;
	struct file *from_file;
	struct file *to_file;


	from_file = tfs_file_open(sbi, from, 0);
	if (!from_file)
		return;
	to_file = tfs_file_open(sbi, to, LOOKUP_CREATE);
	if (!to_file) {
		tfs_close(from_file);
		return;
	}
	
	while ((count = tfs_read(from_file, buf, sizeof(buf))) > 0) {
		count = tfs_write(to_file, buf, count);
		if (count == -1) 
			printf("write error!\n");
		else
			printf("  == %d bytes written!\n", count);
	}

	tfs_close(from_file);
	tfs_close(to_file);
}

void cp_in(struct tfs_sb_info *sbi, char *from)
{
	char buf[1024];
	int count;
	int fd;
	int bytes_written;
	struct file *file;

	file = tfs_file_open(sbi, from, LOOKUP_CREATE);
	if (!file)
		return;
	
	fd = open(from, O_RDONLY);
	if (fd < 0) {
		printf("open file %s error!\n", from);
		return;
	}
	while ((count = read(fd, buf, sizeof(buf))) > 0) {
		bytes_written = tfs_write(file, buf, count);
		if (bytes_written == -1) 
			printf("write error!\n");
		else
			printf("  == %d bytes written!\n", bytes_written);
	}

	tfs_close(file);
}

void cp_out(struct tfs_sb_info *sbi, char *to)
{
	char buf[1024];
	int count;
	int fd;
	int bytes_written;
	struct file *file;

	file = tfs_file_open(sbi, to, 0);
	if (!file)
		return;
	
	fd = open(to, O_RDWR);
	if (fd < 0) {
		printf("open file %s error!\n", to);
		return;
	}
	while ((count = tfs_read(file, buf, sizeof(buf))) > 0) {
		bytes_written = write(fd, buf, count);
		if (bytes_written == -1) 
			printf("write error!\n");
		else
			printf("  == %d bytes written!\n", bytes_written);
	}

	tfs_close(file);
}

int main(int argc, char *argv[])
{
	struct tfs_sb_info *sbi;
	char *fs;
	char cmd[128];

	if (argc != 2) {
		printf("Usage: tfsh fs!\n");
		return -1;
	}


	fs = argv[1];
	sbi = tfs_mount_by_fsname(fs);
	if (!sbi) {
		printf("tfs mount failed!\n");
		return 1;
	}

	cache_init(sbi);

	/* init the this_dir */
	this_dir = tfs_opendir(sbi, "/");
	if (!this_dir) {
		printf("reading '/' as this_dir error!\n");
		return -1;
	}

	while (1) {
		char c;
		char *p = cmd;
		char *opt;

		printf("> ");

		while((c = getchar()) != '\n')
			*p++ = c;
		*p = 0;
		
		p = cmd;
		while (*p != ' ' && *p)
			p++;
		if (*p) {
			*p++ = 0;
			while (*p == ' ')
				p++;
			opt = p;
		} else 
			opt = NULL;
			
		
		if (strcmp(cmd, "cp_in") == 0) {
			printf("copying in '%s' ... \n", opt);
			cp_in(sbi, opt);
		} else if (strcmp(cmd, "cp_out") == 0) {
			printf("copying out file '%s' ...\n", opt);
			cp_out(sbi, opt);
		} else if (strcmp(cmd, "cp") == 0) {
			printf("copying file '%s' to '%s' ...\n", opt, opt);
			cp(sbi, opt, opt);
		} else if (strcmp(cmd, "cat") == 0) {
			printf("cating file '%s' ... \n", opt);
			cat(sbi, opt);
		} else if (strcmp(cmd, "cd") == 0) {
			printf("cd in '%s' ... \n", opt);
			cd(sbi, opt);
		} else if (strcmp(cmd, "echo") == 0) {
			printf("%s\n", opt);
		} else if (strcmp(cmd, "ls") == 0) {
			if (!opt)
				opt = ".";
			printf("listing file '%s' ...\n", opt);
			ls(sbi, opt);
		} else if (strcmp(cmd, "mkdir") == 0) {
			printf("making dir '%s' ... \n", opt);
			cmd_mkdir(sbi, opt);
		} else if (strcmp(cmd, "rmdir") == 0) {
			printf("remoing dir '%s' ... \n", opt);
			cmd_rmdir(sbi, opt);
		} else if (strcmp(cmd, "rm") == 0) {
			printf("removing file '%s' ...\n", opt);
			tfs_unlink(sbi, opt);
		} else if (strcmp(cmd, "touch") == 0) {
			printf("touching file '%s' ...\n", opt);
			touch(sbi, opt);
		} else if (strcmp(cmd, "quit") == 0 || 
			   strcmp(cmd, "exit") == 0) {
			break;
		} else {
			printf("unknow command!\n");
		}
	}

	free(sbi);

}
